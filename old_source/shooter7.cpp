#include <iostream>/*for cin & cout*/
#include <stdlib.h>/*for srand (random numbers)*/
#include <conio.h>/*for colors as words (WHITE) and getch()*/
#include <time.h>/*helps with srand*/
#include <dos.h> /*for delay*/
using namespace std;
using std::cout;
//#include "mode13h.cpp"
// things to do: turn line into more bullet like thing
// real graphics (320x200 or 640x480), firing and moving at the same time
// finish slowing firing down
void ShowScore(int p1, int p2)
{
clrscr();
cout <<	"Player 1: " <<	p1 << endl;
cout <<	"Player 2: " <<	p2 << endl;
}

void ShowPlayers(int p1x, int p1y, int p2x, int p2y)
{
      /*the players' icons*/
      ScreenPutChar('*', (RED),	 p1x, p1y);
      ScreenPutChar('*', (BLUE), p2x, p2y);
}

class player /*Holds the player information*/
{
 public:
  int x,y;  /*xy coordinates*/
  bool alive; /*izzy dead? tells us if we're alive or not (or it will anyway)*/
  int score; /*score*/
};
player p1, p2; // Maybe someday i'll get rid of the globals

struct event;
int op, p1x, p1y, p2x, p2y;

  bool fireup(int x1, int y1, int x2, int y2)
  {
   int fy=y1; /*make a second y for firing, because we need the first for the player*/
   while(fy!=0) /*shot stops at the edge of the screen*/
    {
     delay(2); /*before i learned this command i ran a for loop 5,000,000 times to slow it down*/
     ScreenPutChar('|',	(WHITE), x, fy-1); /*the shot itself, looks like this, goes here*/
     fy--; /*we want the shot to move right?*/
     delay(1); /*this is to further slow down the shot*/
     clrscr(); /*if this isnt here, the shot is a line across the whole screen*/
     ShowPlayers(x1, y1, x2, y2); /*people kept dissapearing, this helped*/
     if (fy==y2) && (x1==x2) /*did the shot hit the other player? if so, */
     {return true;}
     if (kbhit())
     {
     thisop=getch();
     event(thisop, x1, y1, x2, y2);
     }
   }
  clrscr();
  return false;
}

bool firedown(int x, int y, int a, int b)
{
  int fy=y;
    while (fy!=25)
    {
      delay(2);
      ScreenPutChar ('|', (WHITE), x, fy+1);
      fy++;
      delay(3);
      clrscr();
      ShowPlayers(x1, y1, x2, y2);
       if (fy==b && x==a)
	 {return true;}
       if (kbhit())
       {
       thisop=getch();
       event(thisop, x1, y1, x2, y2);
       }
    }
clrscr();
return false;
}

bool fireleft(int x, int y, int a, int b)
{
  int fx=x;
    while (fx!=0)
    {
      delay(1);
      ScreenPutChar ('-', (WHITE), fx-1, y);
      fx--;
      delay(1);
      clrscr();
      ShowPlayers(x1, y1, x2, y2);
       if (fx==a && y==b)
	{return	true;}
       if (kbhit())
       {
       thisop=getch()
       event(thisop, x1, y1, x2, y2);
       }
}
clrscr();
return false;
}

bool fireright(int x, int y, int a, int b)
{
  int fx=x;
   while (fx!=80)
    {
      delay(1);
      ScreenPutChar ('-', (WHITE), fx+1, y);
      fx++;
      delay(1);
      clrscr();
      Game2();
      ShowPlayers(x, y, a, b);
       if (fx==a) && (y==b)
	{return	true;}
      }
clrscr();
return false;
}

  switch(op)
  {
  case 75: p1.x--; clrscr();break;//left
  case 77: p1.x++; clrscr();break;//right
  case 72: p1.y--; clrscr();break;//up
  case 80: p1.y++; clrscr();break;//down

// player 1 firing

  case 105: if (fireup(x1, y1, x2, y2))
	      {p2.alive = 0;p1.score++;}
	    break;

  case 107: if (firedown(x1, y1, x2, y2))
             {p2.alive = 0;p1.score++;}
            break;

  case 108: if (fireright(x1, y1, x2, y2))
	     {p2.alive = 0;p1.score++;}
	    break;

  case 106: if (fireleft(x1, y1, x2, y2))
	     {p2.alive = 0;p1.score++;}
	    break;

// player 2 movement

 case 102: p2.x--; clrscr(); break;//left
 case 104: p2.x++; clrscr(); break;//right
 case 116: p2.y--; clrscr(); break;//up
 case 103: p2.y++; clrscr(); break;//down

// player 2 firing

 case 119: if (fireup(x2, y2, x1, y1))
	    {p1.alive = 0;p2.score++;}
	  break;

 case 115: if (fire.down(x2, y2, x1, y1))
	    {p1.alive = 0;p2.score++;}
	  break;

 case 100: if (fire.right(x2, y2, x1, y1))
	    {p1.alive = 0;p2.score++;}
	  break;

 case 97: if (fire.left(x2, y2, x1, y1))
	   {p1.alive = 0;p2.score++;}
	  break;

 default: clrscr(); ShowPlayers(); break;
   }//end switch

}; // end class definition

void StayOnScreen()   /*keeps the players on screen*/
{
 while (p1.x < 1)
  {p1.x++;}
 while (p1.y < 1)
  {p1.y++;}
 while (p1.x > 79)
  {p1.x--;}
 while (p1.y > 22)
  {p1.y--;}
 while (p2.x < 1)
  {p2.x++;}
 while (p2.y < 1)
  {p2.y++;}
 while (p2.x > 79)
  {p2.x--;}
 while (p2.y > 22)
  {p2.y--;}
}


Game()
{
   if (kbhit())
     {
     op=getch();
     event(op, p1.x, p1.y, p2.x, p2.y);
     }
}





/*     BEGIN MAIN    */

void main()
{
  p1.score = 0;
  p2.score = 0;
  clrscr();
  cout << "Player1
           move:         up, down, left,  right
           firing:       i,    k,   j,     l \n";
  cout << "Player2
                         up down left right
           move:         t   g    f    h
           firing:       w   s    a    d\n Escape Exits";

  cout << "\nEnter the maximum amount of frags: ";
 int maxfrag;
  cin >> maxfrag;
  cout << "\nPress Any Key to Continue. \n";

  while (true)
    {
     if (getch())
     {break;}
    }
  while	(p1.score < maxfrag && p2.score	< maxfrag)
  /*This will reset p1 and p2 once they die, so the game goes on until the fraglimit*/ 
    {

     p1.alive=1;
     p2.alive=1;

      srand((unsigned)time(NULL)); /*random starting position*/
      p1.x=(rand()%77+1);
      p1.y=(rand()%22+1);
      p2.x=(rand()%77+1);
      p2.y=(rand()%22+1);
      clrscr();
      ShowScore(p1.score, p2.score);

    while	(p1.alive && p2.alive)
      {
        ShowPlayers(p1.x, p1.y, p2.x, p2.y);
//      ScreenPutString("Shooter by Curt", 135, 32, 0); /* show game is by me :) */
//      ScreenSetCursor(100,100);	/*cursor off screen*/
        Game();	 /*the game itself*/
        ShowScore(p1.score, p2.score);/*displays the scores on the screen*/
        StayOnScreen();	 /*keep'em on the screen*/
//        if (move == 27)	 /* escape = exit */
//        {return 0;}

// end of the main main part of the game
      }
// end of the reset, and therefore, the rest of the game
    }
// end of program
}
