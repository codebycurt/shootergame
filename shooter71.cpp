#include <stdlib.h>  /* for srand (random numbers) */
#include <time.h>    /* helps with srand */
#include <curses.h>  /* "graphics" library for terminal */
#include <string>    /* I needed this to convert [int] -> [string] -> [char array] */
#include <cstring>   /* to convert c++ strings to c strings for ncurses */

#ifdef _WINDOWS
#include <windows.h>
#else
#include <unistd.h>  /* for sleep */
#define Sleep(x) usleep((x)*1000) /* make "Sleep" Windows & Linux compatible. */
#endif

using namespace std;

class player /*Holds the player information*/
{
 public:
  int x, y;
  bool alive;
  int score;
};


/* TODO: bullets as independent objects, collision detection */
class bullet
{
  public:
    int x, y;
};

/* GLOBAL VARIABLES */

player p1, p2; // Maybe someday i'll get rid of all globals.

int maxfrag;
int xMax, yMax;  // variables for screen size
char bullet = 'o';  // defined here to make it easier to change across the firing functions
int y_firing_range = 5; // how far does the bullet travel?
int x_firing_range = y_firing_range+3;
int delayTime = 30; /// milliseconds

/* FUNCTIONS */

void cleanExit() {
  clear();
  mvprintw(yMax/2-8, xMax/2-17, "Hope it was fun!");
  mvprintw(yMax/2-6, xMax/2-17, "Final Score:  ");
  mvprintw(yMax/2-5, xMax/2-17, "Player 1 || Player 2");
  mvprintw(yMax/2-4, xMax/2-13, to_string(p1.score).c_str());
  mvprintw(yMax/2-4, xMax/2-2, to_string(p2.score).c_str());
  mvprintw(yMax/2-1, xMax/2-17, "Press Any Key");
  nodelay(stdscr, FALSE);
  getch();
  endwin();
  exit(0);
}


void ShowScore(int score_p1, int score_p2, int fraglimit)
{
  mvaddstr(0, 0, "(ESC Exits)");
  mvaddstr(2, 0, "Player 1: ");
  mvaddstr(2, 10, to_string(score_p1).c_str());
  mvaddstr(3, 0, "Player 2: ");
  mvaddstr(3, 10, to_string(score_p2).c_str());

  string border;
    
    for (int i=0;i<xMax;i++) {
    border += '-';
    }
  
  mvaddstr(4, 0, border.c_str());  
} 

void ShowPlayers(int p1x, int p1y, int p2x, int p2y)
{
      /*the players' icons*/
      init_pair(2, COLOR_GREEN, COLOR_BLACK);
      attron(COLOR_PAIR(2));  // make Player 1 Red
      mvaddch(p1y, p1x, 'M');
      attroff(COLOR_PAIR(2));

      init_pair(3, COLOR_YELLOW, COLOR_BLACK);
      attron(COLOR_PAIR(3));  // make Player 1 Yellow
      mvaddch(p2y, p2x, 'W');
      attroff(COLOR_PAIR(3));
      ShowScore(p1.score, p2.score, maxfrag);
}

// I know it's a silly way to do this, since all game activity is paused
// when someone fires, but this was easier as a starting point
// TODO: update code so firing sets a "bullet" in motion, but players can still move

bool fireup(int x1, int y1, int x2, int y2) {
  int fy = y1;

  while(fy > 0 && fy > y1-y_firing_range) {
     fy--;
     clear();
     mvaddch(fy, x1, bullet);
     Sleep(delayTime);   
     ShowPlayers(p1.x, p1.y, p2.x, p2.y);
     refresh();
     if (fy==y2 && x1==x2)
       {return true;}
  }
  return false;
}

bool firedown(int x1, int y1, int x2, int y2)
{
  int fy=y1;
    while (fy < yMax && fy < y1+y_firing_range)
    {
      fy++;
      clear();
      mvaddch(fy, x1, bullet);
      Sleep(delayTime);
      ShowPlayers(p1.x, p1.y, p2.x, p2.y);
      refresh();
      if (fy==y2 && x1==x2)
	      {return true;}
    }
clear();
return false;
}

bool fireleft(int x1, int y1, int x2, int y2)
{
  int fx=x1;
    while (fx > 0 && fx > x1-x_firing_range)
    {
      fx--;
      clear();
      mvaddch(y1, fx, bullet);
      Sleep(delayTime);
      ShowPlayers(p1.x, p1.y, p2.x, p2.y);
      refresh();
      if (fx==x2 && y1==y2)
	      {return	true;}
}
return false;
}

bool fireright(int x1, int y1, int x2, int y2)
{
  int fx=x1;
   while (fx < xMax &&  fx < x1+x_firing_range)
    {
      fx++;
      clear();
      mvaddch(y1, fx, bullet);
      Sleep(delayTime);      
      ShowPlayers(p1.x, p1.y, p2.x, p2.y);
      refresh();
       if (fx==x2 && y1==y2)
	{return	true;}
      }
clear();
return false;
}

void action(int op, int x1, int y1, int x2, int y2)
{
  switch(op)
  {

  case 27: cleanExit();break; // ESCAPE exits

// Player 1 Movement

  case KEY_LEFT: p1.x--;break;//left
  case KEY_RIGHT: p1.x++;break;//right
  case KEY_UP: p1.y--;break;//up
  case KEY_DOWN: p1.y++;break;//down

// Player 1 firing

  case 'i': if (fireup(x1, y1, x2, y2))
	          {p2.alive = 0;p1.score++;}
	          break;

  case 'k': if (firedown(x1, y1, x2, y2))
            {p2.alive = 0;p1.score++;}
            break;

  case 'l': if (fireright(x1, y1, x2, y2))
	          {p2.alive = 0;p1.score++;}
	          break;

  case 'j': if (fireleft(x1, y1, x2, y2))
	          {p2.alive = 0;p1.score++;}
	          break;

// player 2 movement

 case 'a': p2.x--;break;//left
 case 'd': p2.x++;break;//right
 case 'w': p2.y--;break;//up
 case 's': p2.y++;break;//down

// player 2 firing

 case 't': if (fireup(x2, y2, x1, y1))
	         {p1.alive = 0;p2.score++;}
	         break;

 case 'g': if (firedown(x2, y2, x1, y1))
    	     {p1.alive = 0;p2.score++;}
    	     break;

 case 'h': if (fireright(x2, y2, x1, y1))
    	     {p1.alive = 0;p2.score++;}
    	     break;

 case 'f': if (fireleft(x2, y2, x1, y1))
	         {p1.alive = 0;p2.score++;}
	         break;

 default: clear(); break;

  }//end switch
}//end function

void StayOnScreen()   /*keeps the players on screen*/
{
 while (p1.x < 2)
  {p1.x++;clear();}
 while (p1.y < 5)
  {p1.y++;clear();}
 while (p1.x > xMax-2)
  {p1.x--;clear();}
 while (p1.y > yMax-2)
  {p1.y--;clear();}
 while (p2.x < 2)
  {p2.x++;clear();}
 while (p2.y < 5)
  {p2.y++;clear();}
 while (p2.x > xMax-2)
  {p2.x--;clear();}
 while (p2.y > yMax-2)
  {p2.y--;clear();}
}

void initialize()
{
  // start the game with no score
  p1.score = 0;
  p2.score = 0;

  // initialize the screen, turn of echoing keystrokes, set window size
  initscr();
  getmaxyx(stdscr, yMax, xMax);
  
  // color the window
  start_color();
  init_pair(1, COLOR_WHITE, COLOR_BLACK);
  wbkgd(stdscr, COLOR_PAIR(1));
  keypad(stdscr, TRUE);
  
}

void screen_autoresize()
{
  int fxMax, fyMax;
  getmaxyx(stdscr, fyMax, fxMax);
  if (fyMax != yMax || fxMax != xMax)
  {
    xMax = fxMax;
    yMax = fyMax;
  }
}

void Game()
{
    int op;
    screen_autoresize();
    ShowScore(p1.score, p2.score, maxfrag); /*displays the scores on the screen*/
    ShowPlayers(p1.x, p1.y, p2.x, p2.y);
    StayOnScreen();  /*keep'em on the screen*/
    if ((op=getch()) == ERR)
    {
     // do nothing, just let the main loop refresh.
    } else {
    action(op, p1.x, p1.y, p2.x, p2.y);      
    clear();
    }
}


/*     BEGIN MAIN    */
int main()
{
  initialize();

  // Get Game Setup
  char show_controls[] = "-----Player1-----                   \n"
                     "move:         up, down, left, right \n"
                     "firing:       i,   k,    j,    l    \n"
                     "-----Player2-----                   \n"
                     "              up down left right    \n"
                     "move:         t   g    f    h       \n"
                     "firing:       w   s    a    d       \n"
                     "Escape Exits                        \n";

  printw("%s", show_controls);
  printw("Enter the maximum amount of frags (1-9): ");

  char input_frag_limit[00];
  getstr(input_frag_limit);
  maxfrag = atoi(input_frag_limit);

  printw("Press Any Key to Begin.");
  getch(); // wait for any user input
  noecho();
  curs_set(FALSE);
  nodelay(stdscr, TRUE); // pass keyb input immediately, don't wait for "enter"

  // Start the Game
  while	(p1.score < maxfrag && p2.score	< maxfrag)
  /*This will reset p1 and p2 once they die, so the game goes on until the fraglimit*/ 
    {
      clear();
      srand((unsigned)time(NULL)); /*random starting position*/
      p1.x=(rand()%(xMax)+1);
      p1.y=(rand()%(yMax)+1);
      p2.x=(rand()%(xMax)+1);
      p2.y=(rand()%(yMax)+1);
      p1.alive=1;
      p2.alive=1;
      
    while	(p1.alive && p2.alive)
      {
        Game();	 /*the game itself*/
      }  // end of the main main part of the game
    } // end of the frag reset, and therefore, the rest of the game
cleanExit();// end of program
return 0;
}